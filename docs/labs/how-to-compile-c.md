# How to compile C project

This is an introductory example. The purpose of this example is to show
what is needed to compile a C project with dependencies, and what
technologies we use to simplify that.


# Task

Write program `sum-a-b` that sums two floating-point numbers stored in
`sum-input.json` file.

To parse a JSON file, we will use the [`json-c` library][libjson-c].

[libjson-c]: https://github.com/json-c/json-c

In the following sections, we describe how to build the project solving
the task above from scratch, what are the difficulties with this
approach, and how to use Nix and Meson to simplify the build.


# Build from scratch


## Build the dependency

Our program depends on the `json-c` library to parse JSON files, so we
need to get that
library first. We build the library from the source code.

From the library's README, the prerequisities are `gcc` and `cmake`. We
are lucky enough to have them installed on our system.

The build instructions are:

    git clone https://github.com/json-c/json-c.git
    mkdir json-c-build
    mkdir json-c-install
    cd json-c-build
    cmake ../json-c -DCMAKE_INSTALL_PREFIX=../json-c-install
    make
    make install

Row-by-row, these instructions do:

1. Copy the library source code using `git`.
2. Create build directory for the library.
3. Create install directory for the library.
4. Change to the build directory.
5. Call the `cmake` program with the argument where to install the
   library. (CMake is build system used to automate steps for building
   the project.)
6. Build the library using `make`.
7. Copy to be used files of the library to the install location defined
   above.


## Write the code

We will put our code in it's own directory:

    mkdir esw-intro
    cd esw-intro

The source code is rather trivial, see `sum-a-b.c`:

    #include <stdio.h>
    #include "json-c/json.h"

    int
    main(void)
    {
        json_object *root = json_object_from_file("sum-input.json");
        if (!root) {
            exit(1);
        }

        json_object *obj_a, *obj_b;
        obj_a = json_object_object_get(root, "a");
        obj_b = json_object_object_get(root, "b");

        double a, b;
        a = json_object_get_double(obj_a);
        b = json_object_get_double(obj_b);

        printf("%f + %f = %f\n", a, b, (a + b));
        return 0;
    }

And there is also an example JSON input file `sum-input.json`:

    {
        "a": 2.5,
        "b": 3
    }


## Build the program

Our goal is to create `sum-a-b` executable that loads the
`sum-input.json` file and outputs the sum of two numbers from that file.
Generally, we call this process *building the project*.

Often, *building* and *compiling* is used interchangeably.

We will build `sum-a-b` executable using `gcc`:

    gcc sum-a-b.c -I../json-c-install/include -L../json-c-install/lib -ljson-c -osum-a-b

Let us briefly discuss the `gcc`'s arguments:

- `sum-a-b.c` is the file with the source code
- `-I../json-c-install/include` specify where to (also) find header
  files; header files of standard libraries are searched
  automatically, usually in `/usr/include`
- `-L../json-c-install/lib` specify where to (also) find libraries;
  standard libraries (e.g., libc) are linked automatically, when needed
- `-ljson-c` specify to link the `libjson-c` library
- `-osum-a-b` specify the name of the executable

Finally, run the executable:

    $ ./sum-a-b
    2.500000 + 3.000000 = 5.500000


# Two main issues

There are two main issues with approach described above. First, the
dependencies. Dependencies speed up the development process allowing
code re-use. However, the question how to distribute these dependencies
remain. The most common is distributing packages within an operating
system's package manager. We also know language-specific package
managers like *Conan* for C/C++ or *PyPI* for Python. In our labs, we will use
*Nix*, modern approach to package management promising reproducible
builds and working in all Linux distributions.

The second issue corresponds to the arguments of `gcc` in the example
above. Writing compilation commands manually to build the project is
not scalable as the number of the arguments grows really fast. The
solution is a high-level build system. Well-known build systems are *make* and
*CMake*. In the following lectures, we will use *Meson*, the build
system promising to be as user-friendly as possible.

# Build with Nix and Meson

Along with the file with the source code `sum-a-b.c` and the file with
the input `sum-input.json`, we need configuruation files for Nix and
Meson. These two files will be provided in the templates for the
following lectures.

Nix allows us to install the needed dependencies automatically,
independently of Linux distribution. For Nix to do that, we need a
`shell.nix` file that contains the list of packages our project
depends on:

    with (import <nixpkgs> {});
    mkShell {
        buildInputs = [
            pkg-config
            meson
            ninja
            json_c
        ];
    }

Package names can be found on <https://search.nixos.org/packages>.

Meson automates software configuration, building and installing. For
Meson, we need a `meson.build` file that describes how to build our
project:

    project('sum-a-b', 'c',
      default_options: [
        'warning_level=3',
        'werror=true',
        'optimization=2',
        'debug=true'
    ])
    libjsonc = meson.get_compiler('c').find_library('json-c')
    executable('sum-a-b', 'sum-a-b.c', dependencies: [libjsonc])

Finally, it is also a good idea to put all the information needed into
the `README` file:

    To build the project, install dependencies or use Nix shell:

        nix-shell

    Then build the project with meson:

        meson setup builddir
        meson compile -C builddir

    And finally run the project:

        ./builddir/sum-a-b

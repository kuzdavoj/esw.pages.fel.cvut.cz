# B4M36ESW - Efficient Software

Within the course of [Efficient software][] you will get familiar with
the area of software and algorithm optimization under limited resources.
The course is focused on the efficient usage of modern hardware
architectures - multi-core and multi-processor systems with shared
memory. Students will practically implmenet and use presented techniques
in C and Java. Main topics are: code optimization, efficient data
structures and processor cache usage, data structures in multi-threaded
applications, and implementation of efficient network servers.

[Efficient software]: https://fel.cvut.cz/cz/education/bk/predmety/47/01/p4701906.html


## Assessment

Graded assessment is based on the point evaluation, where the final
grade is given by the evaluation scale [The study and examination code
of CTU][ctu exam code], article 15. A student can obtain 60 points for
the work during the practical part of the course and 30 points for the
written final evaluation. Oral examination is optional and a student can
obtain up to 10 points. In order to get zápočet/basic assessment (the
condition prior final examination), a student must have at least 30
points for the work during the [practical part of the course][labs] and
all tasks must be successfully submitted. A student can pass examination
if he/she has at least 20 points from final examination. The final grade
is given by the sum of points according to the evaluation scale.

[ctu exam code]: http://www.fel.cvut.cz/en/education/rules/Study_and_Exam_Code.pdf
[labs]: #labs


## Example of exam test

Time limit for both parts is 60 minutes.

- [JAVA part test example][java test]
- [C/C++ part test example][c test]

[java test]: pdfs/example_exam_test_java.pdf
[c test]: pdfs/zkouska_2018-09-06en.pdf


## Lectures

| #   | Date&nbsp;&nbsp; | Topics                                                                                                                                                                                                                     | Slides                                                                                                        |
|-----|-----------------:|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 1.  |           20. 2. | Introduction, modern computer architecture, C compilers.                                                                                                                                                                   | [slides](pdfs/esw01-intro.pdf)                                                                                |
| 2.  |           27. 2. | Bentley's rules, C compiler, profiling.                                                                                                                                                                                    | [slides 1](pdfs/esw02-bentley.pdf), [slides 2](pdfs/esw02-compiler.pdf), [code](assets/esw02_2017_src.tar.gz) |
| 3.  |            6. 3. | Benchmarking, measurements, metrics, statistics, WCET, timestamping.                                                                                                                                                       | [slides](pdfs/esw02-benchmarking.pdf)                                                                         |
| 4.  |           13. 3. | Scalable synchronization – from mutexes to RCU (read-copy-update), transactional memory, scalable API.                                                                                                                    | [slides](pdfs/esw04-synchronization.pdf)                                                                      |
| 5.  |           20. 3. | Memory access – efficient programming with caches, dynamic memory allocation (malloc, NUMA, ...).                                                                                                                         | [slides](pdfs/esw06-memory-access.pdf), [animations](assets/esw08-animations.tar.gz)                          |
| 6.  |           27. 3. | Serialization of data structures – JSON, XML, protobufs, AVRO, cap'n'proto, mmap/shared memory.                                                                                                                           | [slides](pdfs/esw07-serialization.pdf)                                                                        |
| 7.  |            3. 4. | Program run – virtual machine, byte-code, Java compiler, JIT compiler, relation to machine code, byte-code analysis, dissasembly of Java byte-code, optimization in compilers, program performance analysis, profiling.   | [slides](pdfs/esw05_2022.pdf)                                                                                 |
|     |           10. 4. | Easter                                                                                                                                                                                                                     |                                                                                                               |
| 8.  |           17. 4. | Data concurrency in JVM – multi-threaded access to data, locks monitoring, atomic operations, lock-less/block-free data structures, non-blocking algorithms (queue, stack, set, dictionary), data races, synchronization. | [slides](pdfs/esw08_2022.pdf)                                                                                 |
| 9.  |           24. 4. | Efficient servers, C10K, non-blocking I/O, efficient networking.                                                                                                                                                           | [slides](pdfs/esw09_2022.pdf)                                                                                 |
| 10. | 4. 5. (Thursday) | JVM – Memory analysis (dynamic/static), data structures, collections for performance.                                                                                                                                     | [slides](pdfs/esw10_2022.pdf)                                                                                 |
| 11. |  9. 5. (Tuesday) | JVM – Object allocation, bloom filters, references, effective caching.                                                                                                                                                    | [slides](pdfs/esw11_2022.pdf)                                                                                 |
| 12. |           15. 5. | Memory Management in JVM – Memory Layout, Garbage Collectors.                                                                                                                                                              | [slides](pdfs/esw12_2022.pdf)                                                                                 |
| 13. |           22. 5. | Virtualization (IOMMU, SR-IOV, PCI pass-through, virtio, …).                                                                                                                                                               | [slides](pdfs/esw13-virtualization.pdf), [code](assets/vmm.tar.gz)                                            |


## Labs

You will be given nine tasks during the semester. Eight simpler tasks
are scored by 5 points per task. The ninth task is the semestral work
scored by 20 points, including functionality and code quality.

| Task                                                       | Release | Deadline | Presentation | Max. points | Min. points |
|------------------------------------------------------------|---------|----------|--------------|-------------|-------------|
| [How to compile C project](labs/how-to-compile-c.md)       | 20. 2.  |          |              |             |             |
| [Profiling C/C++](labs/profiling-c.md)                     | 27. 2.  | 13. 3.   |              | 5           |             |
| [Benchmarking (Java)](labs/benchmarking.md)                | 6. 3.   | 20. 3.   |              | 5           |             |
| [Read Copy Update (C/C++)](labs/rcu.md)                    | 13. 3.  | 27. 3.   |              | 5           |             |
| [Asynchronous I/O](labs/event-handling.md)                 | 20. 3.  | 3. 4.    |              | 5           |             |
| [Serialization](labs/serialization.md)                     | 27. 3.  | 17. 4.   |              | 5           |             |
| [Profiling Java](labs/profiling-java.md)                   | 3. 4.   | 24. 4.   |              | 5           |             |
| Easter                                                     | 10. 4.  |          |              |             |             |
| [Non-blocking Algorithms](labs/non-blocking-algorithms.md) | 17. 4.  | 4. 5.    |              | 5           |             |
| [Efficient servers](labs/efficient-servers.md)             | 24. 4.  | 14. 5.   | 15. 5.       | 20          |             |
| [Java Vector API](labs/java-vector-api.md)                 | 9. 5.   | 22. 5.   |              | 5           |             |
| **Total**                                                  |         |          |              | **60 (+8)** | **30**      |

Tasks are submitted to your tutor (and into the upload system as well).
The maximum amount of points will be given when the task is uploaded in
time, otherwise, penalty points will be applied – one point per week for
simpler tasks and two points per week for more complex tasks will be
deducted from the total task score. Simpler tasks submitted after
solution presentation (4-5 weeks after the release) will be rewarded by
0 points (**you still have to submit it**).

All the tasks have to be submitted before the start of the examination
period.

Lab attendance is recorded (voluntary). However, the order of
consultations and submissions during labs is based on your attendance.

If you have any questions, please ask via [GitLab Issues][].

In order to obtain zápočet/basic assessment (the condition prior to
final examination), all tasks must be submitted with a sum of at least
30 points.

[GitLab Issues]: https://gitlab.fel.cvut.cz/esw/esw.pages.fel.cvut.cz/-/issues


### Contacts

- [GitLab Issues (preferred)][gitlab issues]
- Lecturers:
    - [Ing. Michal Sojka, Ph.D.](mailto:Michal.Sojka@cvut.cz)
    - [doc. Ing. David Šišlák, Ph.D.](mailto:sislakd@fel.cvut.cz)
- Practising:
    - [Ing. Marek Cuchý (Java)](mailto:marek.cuchy@fel.cvut.cz)
    - [Ing. Jiří Vlasák (C/C++)](mailto:jiri.vlasak.2@cvut.cz)


## Literature

- MIT: Performance-engineering-of-software-systems
- Oaks, S.: Java Performance: 2nd Edition. O'Reilly, USA 2020.
- Jones, R., Hosking, A., Moss, E.: The Garbage Collection Handbook --
  The Art of Automatic Memory Management. CRC Press, USA 2012.
- Herlihy, M., Shavit, N.: The Art of Multiprocessor Programming. Morgan
  Kaufman, 2008.
- Fog, A.: The microarchitecture of Intel, AMD and VIA CPU, 2016.
  ([online](http://www.agner.org/optimize/microarchitecture.pdf))
- Drepper U.: What every programmer should know about memory, 2007
- Jain, R.: The Art of Computer Systems Performance Evaluation. Wiley,
  New York 1991. (slides, book)
- Lilja, D. J.: Measuring Computer Performance: A Practitioner's Guide.
  Cambridge University Press, 2000. (book web site, Supplemental
  Teaching Materials)

# Profiling in C/C++


## Your own machine with Nix
The most comfortable way. Use `nix-shell` to get the environment with
all the dependencies needed (perf, hotspot, libopencv, libboost).


## Your own machine with Linux OS
You need to install (only a few) required tools from the repository
(perf, hotspot, libopencv, libboost). Mac users may use *instruments* or
*Clien* for profiling instead of perf and hotspot.


## Remote access to our server
You may `ssh -X <username>@ritchie.ciirc.cvut.cz` to connect remotely to
the Ritchie server, where Nix is installed. Then, work as you would work
on *your own machine with Nix*.


## Computers in the laboratory
Nix is installed on the computers in the laboratory. Use `nix-shell` to
get the environment with all the dependencies needed (perf, hotspot,
libopencv, libboost).


# Task assignment
Imagine that you are a developer in a company which develops autonomous
driving assistance systems. You are given an algorithm which doesn't run
as fast as your manager want. The algorithm finds ellipses in given
picture and will be used for wheels detection of a neighbouring car
while parking. Your task is to speed up this algorithm in order to run
smoothly.

You probably need do following steps to achieve the desired speed-up:

1. Download the project from git repository

        git clone https://gitlab.fel.cvut.cz/esw/ellipses.git

2. Follow the build instructions in the `README` file.

3. Run the program. Example images are attached in the repository, press
   `q` to quit the GUI mode.

4. Do profiling (hints below).

5. Make changes which will improve speed (code & compiler
   optimizations).

6. Upload a patch file into the [upload system][] and pass specified
   limit: The patch will be applied using the `git apply` command,
   therefore, the best way to generate the patch is the `git diff`
   command:

        git fetch origin && git diff origin/master > ellipse.diff.txt

    (`.txt` file extension because of the upload system.)

**NOTE:** You are not allowed to modify the number of iterations and
other parameters of the RANSAC algorithm!

When compiling the project, use `meson compile --verbose -C builddir` to
see the compiler options used. Investigate those arguments.

To experiment with the compiler flags, create new builddir with no
default options:

    meson setup improved-builddir --buildtype=plain
    meson compile --verbose -C improved-builddir/

and add compiler options of your wishes:

    meson configure improved-builddir -Dcpp_args='-O0 -g'
    meson compile --verbose -C improved-builddir/

**NOTE:** You may also need to add linker options like
`-Dcpp_link_args='-pg'`.


[upload system]: https://cw.felk.cvut.cz/brute/student/course/B4M36ESW/ellipses


# Basic profiling techniques
How can we evaluate the efficiency of our implementation? Run time gives
a simple overview of the program. However, much more useful are
different types of information such as the number of performed
instructions, cache misses, or memory references in respective lines of
code in order to find hot spots of our program.


## Measuring execution time
The easiest way to analyze program performance is to measure its
execution time. There are multiple ways how it can be done in a C/C++
program:

- the [C time library][], specifically the [clock function][]. Note that
  in some systems (other than Linux), the clock resolution may not be as
  good as the options below.

- More precision values can be obtained by using `high_resolution_clock`
  in [chrono C++11 library][].

- On Linux, you can use directly the [`clock_gettime`][clock_gettime]
  function (and system call), which is used by the above options (on
  Linux).


[C time library]: http://www.cplusplus.com/reference/ctime/
[clock function]: https://www.cplusplus.com/reference/ctime/clock/
[chrono C++11 library]: http://www.cplusplus.com/reference/chrono/
[clock_gettime]: https://man7.org/linux/man-pages/man3/clock_gettime.3.html


## GProf
[GProf][] is a GNU profiling tool based on statistical sampling (every 1
ms or 10 ms). It collects the time spent in each function and constructs
call graph. A program must be compiled with a specific option and all
libraries you want to profile must be statically linked. When you then
run the program, profiling information is generated. Note that the
resulting data is not exact. Shared libraries can be profiled with
[sprof][].


[GProf]: https://sourceware.org/binutils/docs/gprof/
[sprof]: http://man7.org/linux/man-pages/man1/sprof.1.html


To use GProf:

1. Set proper compiler and linker arguments to build the project with
   GProf included.
2. Run the program. It will create `gmon.out` if you did (1) well.
3. Examine the output. It is better to first redirect the output to a
   file with `gprof ... > somefile`, and see the `somefile` afterwards.
4. To see the callgraph, use `gprof --graph ... > somefile`.


## Valgrind
[Valgrind][] is a simulation tool for debugging memory and profiling.
Because it is a simulation tool, the runtime is longer and the results
do not need to correspond to the results from the real hardware.

[Cachegrind][] simulates behavior of the program under test in the
relevance to the cache memory.

[Callgrind][] simulates the program under test, recording the calls to
the program's functions.

Outputs from these tools are the best viewed in the [KCachegrind][]
tool.


[valgrind]: https://valgrind.org/
[cachegrind]: http://valgrind.org/docs/manual/cg-manual.html
[callgrind]: https://valgrind.org/docs/manual/cl-manual.html
[kcachegrind]: https://kcachegrind.github.io/


To use Cachegrind:

1. Run the program under valgrind with `valgrind --tool=cachegrind ...`.

To use Callgrind:

1. Run the program under valgrind with `valgrind --tool=callgrind ...`.
2. Examine the output with `kcachegrind`.


## Profiling using perf
Most modern processors have performance counters that can count various
hardware events (clock cycles, instructions executed, cache
reads/hits/misses, etc.). [Linux perf][] is able to analyze a program
based on these counters.

You can also use any hardware events listed in the appropriate reference
manual. For Intel processors - Intel® 64 and IA -32 architectures
software developer's manual: Volume 3B (Chapters 18 and 19) - available
at [https://software.intel.com/en-us/articles/intel-sdm][].

If you get rubbish in your perf report, try specifying the event in the
perf record (example: `perf record -e cycles ./program`). A call graph
is also useful: `perf record --call-graph dwarf -e cycles ./program`.

A few examples of perf usage: [http://www.brendangregg.com/perf.html][]


**NOTE:** By default, using performance counters is not allowed without
sudo privileges, so *you cannot run perf on the computers in the lab*.

When working on your own computer, you can enable access for non-sudo
users by:

    echo 1 | sudo tee /proc/sys/kernel/perf_event_paranoid

On the Ritchie server the value is set as the following:

    $ cat /proc/sys/kernel/perf_event_paranoid
    0

Search for the `perf_event_paranoid` in the [kernel documentation][] for
more information.


[linux perf]: https://perf.wiki.kernel.org/index.php/Main_Page
[https://software.intel.com/en-us/articles/intel-sdm]: https://software.intel.com/en-us/articles/intel-sdm
[http://www.brendangregg.com/perf.html]: http://www.brendangregg.com/perf.html
[kernel documentation]: https://www.kernel.org/doc/Documentation/sysctl/kernel.txt


### Hotspot
The perf tool has a GUI called Hotspot that makes it easier to run the
recording and analyze recorded data.

To use Hotspot:

1. Run `hotspot` (and *Record Data*).
2. Specify *Application*, *Parameters*, and *Working Directory* (`./`).
3. *Start Recording*.
4. *View Results*.


## Handling performance counters directly from C/C++ program
If you are interested in performance counters, you can use it directly
from your C/C++ program (without any external profiling tool). See
`perf_even_open` manual page, or use some helper library built on kernel
API (libpfm, PAPI toolkit, etc.)

- [http://man7.org/linux/man-pages/man2/perf_event_open.2.html][]
- [http://perfmon2.sourceforge.net/][]
- [http://icl.cs.utk.edu/papi/index.html][]


[http://man7.org/linux/man-pages/man2/perf_event_open.2.html]: http://man7.org/linux/man-pages/man2/perf_event_open.2.html
[http://perfmon2.sourceforge.net/]: http://perfmon2.sourceforge.net/
[http://icl.cs.utk.edu/papi/index.html]: http://icl.cs.utk.edu/papi/index.html


## Windows alternatives
We have no experience with Windows tools, however, there are a few free
tools, for example, list on this page:

- [https://wiki.qt.io/Profiling_and_Memory_Checking_Tools][]

MS Visual Studio has profiler:

- [https://msdn.microsoft.com/en-us/library/mt210448.aspx][]

Another windows profilers

- [https://sourceforge.net/projects/lukestackwalker/][]
- [http://www.codersnotes.com/sleepy/][]

Also, Windows alternative to KCacheGrind – QCacheGrind:

- [https://sourceforge.net/projects/qcachegrindwin/][]

If you do not want install any of these tools, you can work remotely on
our server via ssh. [Putty][] and [Xming][] are your friends.


[https://wiki.qt.io/Profiling_and_Memory_Checking_Tools]: https://wiki.qt.io/Profiling_and_Memory_Checking_Tools
[https://msdn.microsoft.com/en-us/library/mt210448.aspx]: https://msdn.microsoft.com/en-us/library/mt210448.aspx
[https://sourceforge.net/projects/lukestackwalker/]: https://sourceforge.net/projects/lukestackwalker/
[http://www.codersnotes.com/sleepy/]: http://www.codersnotes.com/sleepy/
[https://sourceforge.net/projects/qcachegrindwin/]: https://sourceforge.net/projects/qcachegrindwin/
[Putty]: http://www.putty.org/
[Xming]: https://sourceforge.net/projects/xming/


## How to optimize execution time?
There are many ways how to optimize your programs, you can

- play with [Options That Control Optimization][] or generate code
  adjusted for your processor, e.g. with [x86 Options][],

- avoid dynamic memory allocations in computation (sometimes, you can
  reuse e.g. existing arrays),

- try to precompute values, replace exact values by approximations,
  simplify equations if possible,

- inline functions,

- parallelize (not in this case),

- …

Several tips for optimizations: [https://people.cs.clemson.edu/~dhouse/courses/405/papers/optimize.pdf][]


[Options That Control Optimization]: https://gcc.gnu.org/onlinedocs/gcc/gcc-command-options/options-that-control-optimization.html
[x86 Options]: https://gcc.gnu.org/onlinedocs/gcc/gcc-command-options/machine-dependent-options/x86-options.html
[https://people.cs.clemson.edu/~dhouse/courses/405/papers/optimize.pdf]: https://people.cs.clemson.edu/~dhouse/courses/405/papers/optimize.pdf

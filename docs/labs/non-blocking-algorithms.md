# Non-blocking Algorithms

Your task is to implement a concurrent set of integers implemented as a
skip list without using locks.


# Task assignment

1. Start with the implementation of the set using simple linked list.

    1. Download a template from
       [https://gitlab.fel.cvut.cz/esw/nonblock-linkedlist](https://gitlab.fel.cvut.cz/cuchymar/nonblock-basic-template.git)

    2. Desing a representation of the node (`AtomicReference`).

    3. Implement the method `add(int value)` for the case that all
       threads only writes to the list (no deletes).

    4. Implement the `delete(int value)` method.

    5. Implement the `delete(int value)` method correctly with marking
       of the deleted node beforehand and with other threads helping
       with the delete if they find any marked node. You will have to
       modify the `Node` class so it is able to work with the marks
       (`AtomicMarkableReference`). Implement a helper method
       `find(int value)`, which traverses the list until it finds the
       correct position of the value and deletes the marked nodes during
       the traversal.

    6. Implement wait-free `contains(int value)` method. The method is
       read-only.

2. Generalize the previous algorithm to a skip list.

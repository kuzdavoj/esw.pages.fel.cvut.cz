This repository contains the web pages of [ESW course][esw].

[esw]: https://esw.pages.fel.cvut.cz/

To develop locally, prepare Python3 virtual environment:

    python3 -m venv tve
    ./tve/bin/pip install mkdocs

And run `mkdocs`:

    ./tve/bin/mkdocs serve

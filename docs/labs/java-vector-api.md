# Java Vector API

Java Vector API provides the ability to explicitly write vector
([SIMD][]) computations that compiles to vector instructions on
supported CPUs.  Although one of the JVM JIT compiler optimizations
(called auto-vectorization) uses the vector instructions too, it cannot
always benefit from them because it cannot know as much about the
algorithm as the programmer.

The feature is still under development; however, it is mature enough to
be a part of the JDK library as an incubator module from Java 16. You
need to explicitly include the module by the following JVM argument:

    --add-modules jdk.incubator.vector

[SIMD]: https://en.wikipedia.org/wiki/Single_instruction,_multiple_data


# Exercises


## Sum of an Array

Try to implement the following code using vector API (solution will be
presented during the seminar and later provided in slides):

	public static int sum(int[] array) {
		int sum = 0;
		for (int i : array) {
			sum += i;
		}
		return sum;
	}


## Dot Product

Implement dot product using vector API. Scalar implementation can be
seen here:

    public static int dot(int[] v1, int[] v2) {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException();
        }
        int sum = 0;
        for (int i = 0; i < v1.length; i++) {
            sum += v1[i] * v2[i];
        }
        return sum;
    }


## Matrix Multiplication

You can also try to improve the multiplication with the second matrix
transposed.


# Materials

The documentation can be seen in the appropriate JEPs: [338][], [414][],
[417][].

Or you can look at this [video][].

Slides from the seminar (will be provided later).

[338]: https://openjdk.java.net/jeps/338
[414]: https://openjdk.java.net/jeps/414
[417]: https://openjdk.java.net/jeps/417
[video]: https://www.youtube.com/watch?v=VYo3p4R66N8&ab_channel=Java
